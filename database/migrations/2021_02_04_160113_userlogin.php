<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Userlogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('Userlogin', function (Blueprint $table) {
            // $table->id();   
            $table->string('user_id');         
            $table->primary('user_id');
            $table->string('user_name')->unique();
            $table->string('user_email');
            $table->string('user_phone');
            $table->string('user_password');
            $table->string('user_type');
            $table->string('user_status');  
            $table->string('create_by');                        
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Userlogin');
    }
}
