<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::dropIfExists('Product');
        Schema::create('Product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name',50)->unique();
            $table->string('product_image');
            $table->string('product_price',25);
            $table->string('product_description',1000);
            $table->string('smallslide',1)->nullable(true);
            $table->string('cards',1)->nullable(true);
            $table->string('article',1)->nullable(true);
            $table->string('active',1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('Product');
        Schema::dropIfExists('Product');
    }
}
