<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sysparam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('System_param', function (Blueprint $table) {
            $table->id();   
            // $table->string('user_id');         
            // $table->primary('user_id');
            $table->string('param_name')->unique();
            $table->string('param_value');               
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('System_param');
    }
}
