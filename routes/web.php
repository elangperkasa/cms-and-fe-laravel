<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// umum
Route::get('/', 'App\Http\Controllers\UserCRUDController@index');
Route::get('/profile', 'App\Http\Controllers\UserCRUDController@viewprofile');
Route::get('/logout', 'App\Http\Controllers\UserCRUDController@logout');

// user
Route::get('/loginpage', 'App\Http\Controllers\UserCRUDController@loginpage');
Route::post('/login', 'App\Http\Controllers\UserCRUDController@login');
Route::get('/signup', 'App\Http\Controllers\UserCRUDController@signup');
Route::post('/registernew', 'App\Http\Controllers\UserCRUDController@registernew');
Route::post('/userfeupdate', 'App\Http\Controllers\UserCRUDController@userfeupdate');

// admin
Route::get('/admin', 'App\Http\Controllers\Controller@index');
Route::get('/userlist', 'App\Http\Controllers\Controller@userlist');
Route::get('/edituser', 'App\Http\Controllers\Controller@edituser');
Route::post('/updateuser', 'App\Http\Controllers\Controller@updateuser');
Route::get('/deleteuser', 'App\Http\Controllers\Controller@deleteuser');
Route::get('/adduser', 'App\Http\Controllers\Controller@adduser');

Route::post('/insertnewuser', 'App\Http\Controllers\Controller@insertnewuser');

// email
Route::get('/sendemail', 'App\Http\Controllers\UserCRUDController@sendemail');
Route::get('/send-email', [MailController::class, 'sendEmail']);
Route::get('/sendemail8', 'App\Http\Controllers\MailController@sendEmail');
// Route::get('/confirmation', 'App\Http\Controllers\Controller@confirmationemail');
Route::get('/confirmation/{id}', 'App\Http\Controllers\Controller@confirmationemail');


Route::get('/clearall', function() {

    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    echo "Cache Clear All";

});

Route::get('/banner', [Controller::class, 'bannerview']);
Route::get('/addbanner', [Controller::class, 'addbanner']);
Route::post('/insertnewbanner', [Controller::class, 'insertnewbanner']);
Route::get('/deletebanner', [Controller::class, 'deletebanner']);
Route::get('/editbanner', [Controller::class, 'editbanner']);
Route::post('/updatebanner', [Controller::class, 'updatebanner']);


Route::get('/products', [Controller::class, 'productsview']);
Route::get('/addproduct', [Controller::class, 'addproduct']);
Route::post('/insertnewproduct', [Controller::class, 'insertnewproduct']);
Route::get('/editproducts', [Controller::class, 'editproducts']);
Route::post('/updateproduct', [Controller::class, 'updateproduct']);
Route::get('/deleteproducts', [Controller::class, 'deleteproducts']);

Route::get('/sysparam', [Controller::class, 'sysparam']);
Route::get('/addparam', [Controller::class, 'addparam']);
Route::post('/insertnewparam', [Controller::class, 'insertnewparam']);