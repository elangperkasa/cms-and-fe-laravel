<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\User_data;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Controllers\FileUpload;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        // $data['users'] = User_data::orderBy('user_id','desc')->paginate(5);
        // return view('welcome', $data);
        $value = "";
        if(isset($_SESSION['loginnya']))
        {
            $value = $_SESSION['loginnya'];
        }

        $user_type="";
        if(isset($_SESSION['user_type']))
        {
            $user_type = $_SESSION['user_type'];
        }

        $users['login']=$value;  
        // $users['user_type']=$user_type;
        // echo $user_type;
        // die;
        if($user_type=='admin')
        {
            // echo "admin";            
            // die;
            return view('admin/admin',['users'=>$users]); 
        }
        else if($user_type=='user')
        {
            // echo "user";
            // die;
            // return view('users/profile',['users'=>$users]); 
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }
        else
        {
            // echo "siapa ini";
            // die;
            // echo "tidak masuk login 2";
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }
    }
    
    public function userlist(Request $request)
    {
        
        $value="";
        // $users['page'] = $request->page;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {
                // $users['usernya'] = DB::select('select * from userlogin'); OK
                // $users['usernya']  = DB::table('userlogin')->paginate(5); OK
                $users['count'] = DB::table('userlogin')->count();
                $users['usernya'] = DB::table('userlogin')
                            // ->join('users', 'notices.user_id', '=', 'users.id')
                            // ->join('departments', 'users.dpt_id', '=', 'departments.id')
                ->select('user_id' , 'user_email','user_password', 'user_name', 'user_type', 'user_status')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);
                $users['count2'] =  count($users['usernya']);
                return view('admin/userlist',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function edituser(Request $request)
    {
        
        $value="";
        // $users['user_id'] = $request->query('id');
        $users['user_id'] = $request->id;
        // echo $users['user_id'] ;
        // die;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {
                $users['usernya'] = DB::select("select * from userlogin where user_id='".$request->id."'"); 
                $users['count'] = count($users['usernya']);
                return view('admin/edituserpage',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function updateuser(Request $request)    {
        
        $value="";
        // $users['user_id'] = $request->query('id');
        // $users['user_id'] = $request->user_id;
        $user_idupdate=$request->user_id;
        // echo "1".$users['user_id'] ;
        // echo "<br>2".$user_id ;
        // die;
        $users['page'] = $request->query('page');

        $user_nameupdate = $request->user_name;            
        // echo "<br>".$user_name;
        $user_phoneupdate = $request->user_phone;
        // echo  "<br>".$user_phone;
        $user_emailupdate = $request->user_email;
        // echo  "<br>".$user_email;
        $user_typeupdate = $request->user_type;
        // echo  "<br>".$user_type;
        $user_statusupdate = $request->user_status;
        // echo  "<br>".$user_status;
        // $user_id = $request->id;
        // echo $user_id;
        // die;

        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
            
         

            if($user_type=='admin')
            {
                
                DB::update('update userlogin set user_name  = ?,user_phone=?,user_email=?,user_type=?,user_status=?,updated_at=? where user_id = ?',[$user_nameupdate ,$user_phoneupdate,$user_emailupdate,$user_typeupdate,$user_statusupdate,date("Y-m-d h:i"),$user_idupdate]);
                // echo "Record updated successfully";,

                // $users['usernya'] = DB::table('userlogin')
                // ->select('user_id' , 'user_email','user_password', 'user_name', 'user_type', 'user_status')
                // ->orderBy('update_date', 'desc')
                // ->paginate(10);
                // $users['count'] =  count($users['usernya']);

                // $users['usernya'] = DB::select("select * from userlogin"); 
                // $users['count'] = count($users['usernya']);
                // return view('admin/userlist',['users'=>$users]);   
                return redirect('/userlist');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function deleteuser(Request $request)    {
        
        $value="";
        $user_id=$request->id;
        // echo $user_id;
        // die;
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }  
                    

            if($user_type=='admin')
            {
                
                DB::delete('delete from userlogin where user_id = ?',[$user_id]);
                // echo "Record deleted successfully.<br/>"; 
                return redirect('/userlist');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function confirmationemail(Request $request)    {
        $user_idupdate = $request->segment(2);
        // echo $user_statusupdate;
        // die;
        
        $value="";       

        $users['login']=$value;
           
                
                DB::update('update userlogin set user_status=?,updated_at=? where user_id = ?',['active',now(),$user_idupdate]);
               
                session()->flash('message', 'Your email confirmed, please login now to access your account');
                return view('users/login',['users'=>$users]); 
           
          
    }

    public function cekuserakses()
    {
        $value="";
        $user_type="";

        if( isset($_SESSION['loginnya']) )
        {
            $value=$_SESSION['loginnya'];
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }

        // $users['login']=$value;

        if ($value=='done'){
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
                // return $user_type;
                return  [$user_type, $value];
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }
    }

    public function adduser(Request $request)
    {        
       
        list($user_type, $value)=self::cekuserakses();
        $users['login']=$value;
        // echo $user_type;
        // echo $users['login'];
        // die;

            if($user_type=='admin')
            {
                return view('admin/adduserpage',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
         
    }

  
    public function insertnewuser(Request $request)
    {
        
       
        $users['usernya']  = DB::select("select * from userlogin where user_email ='".$request->user_email."'");
        if(count($users['usernya'])>0)
        {
            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="User already registered";
            echo "User already registered";
            // session()->flash('message', 'User already registered');
            // return view('users/signup',['users'=>$users]); 
            
        }
        else
        {
            // $contactName = Input::get('user_fullname');
            // $contactEmail = Input::get('user_email');
            // $contactMessage = Input::get('message');
            

            $user_name=$request->user_name;
            // echo "full name=".$user_fullname;
            $user_email=$request->user_email;
            // echo "email=".$user_email;
            $user_phone=$request->user_phone;
            // echo "phone=".$user_phone;
            $user_password=$request->user_password;
            // echo "password=".$user_password;
            $user_type=$request->user_type;
            $user_status=$request->user_status;
            
            $newid=bin2hex(random_bytes(16));
            DB::table('userlogin')->insert([
                'user_id' => $newid,
                'user_name' => $user_name,
                'user_email' => $user_email,
                'user_phone' => $user_phone,
                'user_password' => $user_password,
                'user_type' => $user_type,
                'user_status' => $user_status,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
                'create_by' => "From Web"                
            ]);

            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="cekemail";           
             
            // Mail::send('mailtemplate/mail', $data, function($message)use($data) {
            //     $message->to($data["email"], $data["email"])
            //             ->subject('Please confirm your email');

            //     $message->from('travel@travelcheap.com','Travel Cheap');
            // });

         
            // session()->flash('message', 'Please check your email for confirmation');
            // return view('users/signup',['users'=>$users]); 
            return redirect('/userlist');
        }
    }


    public function bannerview(Request $request)
    {
        
        
        $value="";
        // $users['page'] = $request->page;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {                
                // $users['count'] = DB::table('userlogin')->count();
                $users['users'] = DB::table('banner')                           
                ->select('*')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);
                // $users['count2'] =  count($users['usernya']);
                return view('admin/banner/bannerview',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('controllers/banner/bannerview',['users'=>$users]); 
        }     
    }

    public function addbanner(Request $request)
    {        
       
        list($user_type, $value)=self::cekuserakses();
        $users['login']=$value;
        // echo $user_type;
        // echo $users['login'];
        // die;

            if($user_type=='admin')
            {
                return view('admin/banner/banneradd',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
         
    }

    public function insertnewbanner(Request $request)
    {        
            $banner_name=$request->banner_name;
            // echo "full name=".$user_fullname;
            $banner_url=$request->banner_url;
            // echo "email=".$user_email;
            // $banner_image=$request->file('files') ;
            // echo "email=".$user_email;

            $validator = \Validator::make($request->all(), [
                'files' => 'required'
            ])->validate();
    
            $total_files = count($request->file('files'));
    
            foreach ($request->file('files') as $file) {
                // rename & upload files to uploads folder
                $name = uniqid() . '_' . time(). '.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads';
                $file->move($path, $name);
    
                // store in db
                // $fileUpload = new FileUpload();
                // $fileUpload->filename = $name;
                // $fileUpload->save();
            }
    
            // return back()->with("success", $total_files . " files uploaded successfully");

            // $newid=bin2hex(random_bytes(16));
            DB::table('banner')->insert([
                'banner_name' => $banner_name,
                'banner_url' => $banner_url,
                'banner_image' => $name,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")       
            ]);

            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="cekemail";           
             
            return redirect('/banner');
        
    }

    public function deletebanner(Request $request)    {
        
        $value="";
        $id=$request->id;
        // echo $user_id;
        // die;
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }  
                    

            if($user_type=='admin')
            {                
                DB::delete('delete from banner where id = ?',[$id]);
                // echo "Record deleted successfully.<br/>"; 
                return redirect('/banner');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function editbanner(Request $request)
    {
        
        $value="";
        // $users['user_id'] = $request->query('id');
        $users['user_id'] = $request->id;
        // echo $users['user_id'] ;
        // die;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {
                $users['usernya'] = DB::select("select * from banner where id='".$request->id."'"); 
                $users['count'] = count($users['usernya']);
                return view('admin/banner/banneredit',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function updatebanner(Request $request)    {
        
        $value="";
        // $users['user_id'] = $request->query('id');
        // $users['user_id'] = $request->user_id;
        $id=$request->id;
        // echo "1".$users['user_id'] ;
        // echo "idnya = ".$id ;
        // die;
        $users['page'] = $request->query('page');

        $banner_name = $request->banner_name;            
        // echo "<br>".$user_name;
        $banner_url = $request->banner_url;
        // echo  "<br>".$user_phone;



        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
            
         

            if($user_type=='admin')
            {
                // $validator = \Validator::make($request->all(), [
                //     'files' => 'required'
                // ])->validate();
                // var_dump($request->file('files'));
                // die;
                if($request->file('files')!="" && $request->file('files')!=null)
                {
                    // $total_files = count($request->file('files'));
                        // var_dump($request->file('files'));
                        //         die;

                        foreach ($request->file('files') as $file) {
                            $name = uniqid() . '_' . time(). '.' . $file->getClientOriginalExtension();
                            $path = public_path() . '/uploads';
                            $file->move($path, $name);
                        }
                        DB::update('update banner set banner_name  = ?,banner_url=?,banner_image=?,updated_at=? where id = ?',[$banner_name ,$banner_url,$name,date("Y-m-d h:i"),$id]);
                

                            
                }
                else
                {
        //             echo "null ke 2<br>";
        //             var_dump($request->file('files'));
        // die;
                    DB::update('update banner set banner_name  = ?,banner_url=?,updated_at=? where id = ?',[$banner_name ,$banner_url,date("Y-m-d h:i"),$id]);
                
                }

                return redirect('/banner');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }


    public function productsview(Request $request)
    {
        
        
        $value="";
        // $users['page'] = $request->page;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {                
                $users['products'] = DB::table('product')                           
                ->select('*')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);

                return view('admin/products/productsview',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('controllers/banner/bannerview',['users'=>$users]); 
        }     
    }

    public function addproduct(Request $request)
    {        
       
        list($user_type, $value)=self::cekuserakses();
        $users['login']=$value;
        // echo $user_type;
        // echo $users['login'];
        // die;

            if($user_type=='admin')
            {
                return view('admin/products/productadd',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
         
    }

    public function insertnewproduct(Request $request)
    {        
            $product_name=$request->product_name;
            $product_price=$request->product_price;
            $product_description=$request->product_description;
            $smallslide=$request->smallslide;
            $cards=$request->cards;
            $article=$request->article;
            $active=$request->active;

            $validator = \Validator::make($request->all(), [
                'files' => 'required'
            ])->validate();
    
            $total_files = count($request->file('files'));
    
            foreach ($request->file('files') as $file) {
                $name = uniqid() . '_' . time(). '-product.' . $file->getClientOriginalExtension();
                $path = public_path() . '/uploads';
                $file->move($path, $name);
            }
    
            DB::table('product')->insert([
                'product_name' => $product_name,
                'product_price' => $product_price,
                'product_image' => $name,
                'product_description' => $product_description,
                
                'smallslide' => $smallslide,                
                'cards' => $cards,                
                'article' => $article,                
                'active' => $active,

                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")       
            ]);

            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="cekemail";           
             
            return redirect('/products');
        
    }

    public function editproducts(Request $request)
    {
        
        $value="";
        // $users['user_id'] = $request->query('id');
        $users['user_id'] = $request->id;
        // echo $users['user_id'] ;
        // die;
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {
                $users['usernya'] = DB::select("select * from product where id='".$request->id."'"); 
                $users['count'] = count($users['usernya']);
                return view('admin/products/productsedit',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function updateproduct(Request $request)    {
        
        $value="";

        $id=$request->id;
        $product_name=$request->product_name;
        $product_price=$request->product_price;
        $product_description=$request->product_description;
        $smallslide=$request->smallslide;
        $cards=$request->cards;
        $article=$request->article;
        $active=$request->active;



        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
            
         

            if($user_type=='admin')
            {

                if($request->file('files')!="" && $request->file('files')!=null)
                {

                        foreach ($request->file('files') as $file) {
                            $name = uniqid() . '_' . time(). '-product.' . $file->getClientOriginalExtension();
                            $path = public_path() . '/uploads';
                            $file->move($path, $name);
                        }

                        DB::update('update product set product_name=?,product_price=?,product_image=?,product_description=?,smallslide=?,cards=?,article=?,active=?,updated_at=? where id = ?',[$product_name ,$product_price,$name,$product_description,$smallslide,$cards,$article,$active,date("Y-m-d h:i"),$id]);
                

                            
                }
                else
                {

                    DB::update('update product set product_name=?,product_price=?,product_description=?,smallslide=?,cards=?,article=?,active=?,updated_at=? where id = ?',[$product_name ,$product_price,$product_description,$smallslide,$cards,$article,$active,date("Y-m-d h:i"),$id]);
                
                }

                return redirect('/products');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function deleteproducts(Request $request)    {
        
        $value="";
        $id=$request->id;
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }  
                    

            if($user_type=='admin')
            {                
                DB::delete('delete from product where id = ?',[$id]);
                return redirect('/products');
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }

    public function sysparam(Request $request)
    {
        
        
        $value="";
        $users['page'] = $request->query('page');
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
    
           
            if($user_type=='admin')
            {                
                // $users['count'] = DB::table('userlogin')->count();
                $users['data'] = DB::table('System_param')                           
                ->select('*')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);
                // $users['count2'] =  count($users['usernya']);
                return view('/admin/sysparam',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('controllers/banner/bannerview',['users'=>$users]); 
        }     
    }

    public function addparam(Request $request)
    {        
       
        list($user_type, $value)=self::cekuserakses();
        $users['login']=$value;
        // echo $user_type;
        // echo $users['login'];
        // die;

            if($user_type=='admin')
            {
                return view('admin/addparam',['users'=>$users]);    
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
         
    }

    public function insertnewparam(Request $request)
    {        
            $param_name=$request->param_name;
            $param_value=$request->param_value;
            // echo $param_name;
            // echo $param_value;
            // die;
            // $validator = \Validator::make($request->all(), [
            //     'files' => 'required'
            // ])->validate();
    
                
            DB::table('System_param')->insert([
                'param_name' => $param_name,
                'param_value' => $param_value,
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i")       
            ]);

            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;

            return redirect('/sysparam');
        
    }

}