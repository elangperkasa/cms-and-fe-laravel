<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User_data;
use DB;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Mail;
use Redirect;

class UserCRUDController extends Controller
{
    public function index()
    {
        // session_start();
        // $data['users'] = User_data::orderBy('user_id','desc')->paginate(5);
        // return view('welcome', $data);
        // $_SESSION['loginnya']='done';
        $value = "";
        if(isset($_SESSION['loginnya']))
        {
            $value = $_SESSION['loginnya'];
        }
        $users['login']=$value;  
        $users['user_data'] = DB::select('select * from userlogin');
        
        $users['banner'] = DB::select('select * from banner');

        $users['productsslide'] = DB::select("select * from product where smallslide='Y'");

        return view('welcome',['users'=>$users]);
    }

    public function viewprofile()
    {
        // $value = Session::get('loginnya');
        // $value = session()->get('loginnya');
        // $value = Session::get('loginnya');
        
        $value="";
        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;

        // echo $value;
        $user_type= $_SESSION['user_type'];
        // echo $user_type;
        $user_email=$_SESSION['useremail'];
        //  echo $user_email;
        // die;
        if ($value=='done'){
                    // if($user_type=='admin')
                    // {
                    //     $users['usernya'] = DB::select('select * from userlogin');
                    //     return view('users/profile',['users'=>$users]);  
                    // }
                    // else if($user_type=='user')
                    // {
                        $users['errornya']="";
                        $users['usernya']  = DB::select("select * from userlogin where user_email ='".$user_email."'");
                        return view('users/profile',['users'=>$users]); 
                    // }          
        }
        else
        {
            return view('users/login',['users'=>$users]);
        }       
    }
    
    public function loginpage()
    {
        
        $value="";
        if(isset($_SESSION['loginnya']) )
        {
            $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        return view('users/login',['users'=>$users]);             
    }
    
    public function logout()
    {
        
        $value="out";
        $user_type="";
        $_SESSION['loginnya']="out";
        $value=$_SESSION['loginnya'];
        $_SESSION['user_type']="";
        $users['login']=$value;    
        $users['user_type']=$user_type; 
        return view('users/login',['users'=>$users]);        
    }

    public function login(Request $request)
    {
            $users['usernya']  = DB::select("select * from userlogin where user_email ='".$request->user_email."'");
            $users['login']="belum";   
          

            // echo $request->user_email;
                    // exit;

            if(count($users['usernya'])>0)
            {
                $passnya= $users['usernya'][0]->user_password;
                // echo $passnya;
                $statusnya= $users['usernya'][0]->user_status;
                // echo $statusnya;
                if( $passnya==$request->user_password && $statusnya=="active")
                {                

                    // echo "masuk login";
                    $_SESSION['loginnya']='done';
                    $value = $_SESSION['loginnya'];
                    $users['login']=$value;                        
                    $_SESSION['useremail']=$users['usernya'][0]->user_email;
                    $_SESSION['user_type']=$users['usernya'][0]->user_type;
                    $_SESSION['user_id']=$users['usernya'][0]->user_id;
                    // cek admin atau bukan
                    $user_type= $users['usernya'][0]->user_type;
                    $users['user_type']=$value;
                    // echo $user_type;
                    // exit;

                    if($user_type=='admin')
                    {
                        return view('admin/admin',['users'=>$users]); 
                    }
                    else if($user_type=='user')
                    {
                        // return view('users/profile',['users'=>$users]); 
                        return view('welcome',['users'=>$users]); 
                    }

                }
                else
                {
                    // echo "tidak masuk login 1";
                    // die;
                    session()->flash('message', 'Sorry, your login data is not valid !');
                    return view('users/login',['users'=>$users]); 
                }
            }
            else
            {  
                // echo "tidak masuk login 2";
                // die;
                    session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }    
    }

    public function signup()
    {
        
        $value="";
        if(isset($_SESSION['loginnya']) )
        {
            $value=$_SESSION['loginnya'];
        }
        $users['login']=$value;
        $users['errornya']='';
        return view('users/signup',['users'=>$users]);             
    }

    public function registernew(Request $request)
    {
        
       
        $users['usernya']  = DB::select("select * from userlogin where user_email ='".$request->user_email."'");
        if(count($users['usernya'])>0)
        {
            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="User already registered";
            session()->flash('message', 'User already registered');
            return view('users/signup',['users'=>$users]); 
            // return redirect()->to('users/signup') ;
            
        }
        else
        {
            // $contactName = Input::get('user_fullname');
            // $contactEmail = Input::get('user_email');
            // $contactMessage = Input::get('message');
            

            $user_fullname=$request->user_fullname;
            // echo "full name=".$user_fullname;
            $user_email=$request->user_email;
            // echo "email=".$user_email;
            $user_phone=$request->user_phone;
            // echo "phone=".$user_phone;
            $user_password=$request->user_password;
            // echo "password=".$user_password;
            $newid=bin2hex(random_bytes(16));
            DB::table('userlogin')->insert([
                'user_id' => $newid,
                'user_name' => $user_fullname,
                'user_email' => $user_email,
                'user_phone' => $user_phone,
                'user_password' => $user_password,
                'user_type' => "user",
                'user_status' => "register",
                'created_at' => date("Y-m-d h:i"),
                'updated_at' => date("Y-m-d h:i"),
                'create_by' => "From Web"                
            ]);

            $value="";
            if(isset($_SESSION['loginnya']) )
            {
                $value=$_SESSION['loginnya'];
            }
            $users['login']=$value;
            $users['errornya']="cekemail";

            // send email
            // $user_email=$request->user_email;
            // $data = array('name'=>"Erwin");
            $url="http://localhost:8000/confirmation/".$newid;
            $data = array('name'=>$user_fullname, 'url'=>$url,);
            
            $data["email"] = $request->user_email;
            $data["url"] = $url;
            // $data = array('name'=>$user_email);

            // Mail::send('mailtemplate/mail', $data, function($message) {
            // $message->to($user_email)->subject
            //     ('Please confirm your email');
            // $message->from('no--reply@travelcheap.cong','Travel Cheap');
            // });
            
               
             
            Mail::send('mailtemplate/mail', $data, function($message)use($data) {
                $message->to($data["email"], $data["email"])
                        ->subject('Please confirm your email');

                // foreach ($files as $file){
                //     $message->attach($file);
                // }
                $message->from('travel@travelcheap.com','Travel Cheap');
            });

            // echo "HTML Email Sent. Check your inbox.";


            session()->flash('message', 'Please check your email for confirmation');
            return view('users/signup',['users'=>$users]); 
        }
    }

    public function sendemail() {
        $data = array('name'=>"Erwin");
        Mail::send('mailtemplate/mail', $data, function($message) {
           $message->to('erwin.kurnia@gmail.com', 'Sang Pemuja')->subject
              ('ini namanya siapa?');
           $message->from('erwin.kurnia@gmail.com','Erwin');
        });
        echo "HTML Email Sent. Check your inbox.";
    }


    
    public function userfeupdate(Request $request)    {
        
        $value="";

        $user_nameupdate = $request->user_fullname;            
        // echo "<br>".$user_name;
        $user_phoneupdate = $request->user_phone;
        // echo  "<br>".$user_phone;
        $user_emailupdate = $request->user_email;
        // echo  "<br>".$user_email;
        $user_passwordupdate = $request->user_password;
        // echo $user_passwordupdate;

        $user_passwordconfirmupdate = $request->user_password2;
        // echo $user_passwordconfirmupdate;

        // die;
        // $user_typeupdate = $request->user_type;
        // echo  "<br>".$user_type;
        // $user_statusupdate = $request->user_status;
        // echo  "<br>".$user_status;
        // $user_id = $request->id;
        // echo $user_id;
        // die;

        if( isset($_SESSION['loginnya']) )
        {
        $value=$_SESSION['loginnya'];
        }

        $users['login']=$value;
        if ($value=='done'){
               
            $user_type="";
            if(isset($_SESSION['user_type']))
            {
                $user_type = $_SESSION['user_type'];
            }
            $user_id="";
            if(isset($_SESSION['user_id']))
            {
                $user_id = $_SESSION['user_id'];
            }
            
         
            $passwordsama="T";
            if($user_passwordupdate==$user_passwordconfirmupdate)
            {
                $passwordsama="Y";
            }

            // echo "isinya=".$passwordsama;
            // die;
             
            if($user_id!='' && $user_id!=null)
            {
                if($user_passwordupdate=="")
                {                
                    DB::update('update userlogin set user_name  = ?,user_phone=?,user_email=?,updated_at=? where user_id = ?',[$user_nameupdate ,$user_phoneupdate,$user_emailupdate,date("Y-m-d h:i"),$user_id]);
                    return redirect('/profile');
                }
                else
                {
                    if($passwordsama=="Y")
                    {
                        // echo "pass match";
                        // die;
                        DB::update('update userlogin set user_name  = ?,user_phone=?,user_email=?,user_password=?,updated_at=? where user_id = ?',[$user_nameupdate ,$user_phoneupdate,$user_emailupdate,$user_passwordupdate,date("Y-m-d h:i"),$user_id]);
                        return redirect('/profile');
                    }
                    else
                    {
                        // echo "pass not match";
                        // die;
                        // $users['errornya']="";
                        session()->flash('message', 'Password not match');
                        // return redirect('/profile');
                        // return view('users/profile',['users'=>$users]); 
                        // return Redirect::back()->withErrors(['message', 'Password not match']);
                        // return redirect()->back()->with('message', 'Password not match');
                        // return redirect('/profile')->with('message','Password not match');
                        return redirect('/profile')->with('message','Password not match');
                    }
                }
                
            }       
            else
            {
                session()->flash('message', 'Sorry, your login data is not valid !!');
                return view('users/login',['users'=>$users]); 
            }
        }
        else
        {
            session()->flash('message', 'Sorry, your login data is not valid !!');
            return view('users/login',['users'=>$users]); 
        }     
    }
     
 }
