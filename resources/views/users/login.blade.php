
@include('header')
@extends('users.layout')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.rcorners1 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 500px;
  height: 500px;
  top: 50%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   

#bg {
  position: fixed; 
  top: 0; 
  left: 0; 
	
  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 100%;
  z-index:-100;
}


@media only screen and (max-width: 1024px){
    
    .rcorners1 {
        border-radius: 25px;
      background: #73AD21;
      color: white;
      padding: 15px;
      position: absolute;
      width: 500px;
      height: 500px;
      top: 30%;
      left: 50%;
      -ms-transform: translateX(-50%) translateY(-50%);
      -webkit-transform: translate(-50%,-50%);
      transform: translate(-50%,-50%);
    }   
    #bg {
      position: fixed; 
      top: 0; 
      left: 0; 
      
      /* Preserve aspet ratio */
      min-width: 100%;
      min-height: 1
    }

@media only screen and (max-width: 479px){
    
.rcorners1 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 300px;
  height: 500px;
  top: 50%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   
#bg {
  position: fixed; 
  top: 0; 
  left: 0; 
	
  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 1
}

</style>
<img src="images/images3.jpg" id="bg" alt="">
<div class='rcorners1' >
    <h1>Login Area</h1>
    &nbsp;<br>&nbsp;<br>
    <form action="/login" method="post">
    <div>
        @if (session()->has('message'))
              @if (session('message')=='Your email confirmed, please login now to access your account')
                    <div class="alert alert-success" >
                        <span><b>*{{ session('message') }}</b></span>
                    </div>     
              @else
              <div class="alert alert-danger" >
                  <span style="color:red"><b>*{{ session('message') }}</b></span>
              </div>    
              @endif      
        @endif
    </div>
    {{csrf_field()}}
    @csrf
        <div class="form-group">
            <label for="user_email">Email address</label>
            <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted" style="color:black!important;">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="user_password">Password</label>
            <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Password">
        </div>
      <button type="submit" class="btn btn-primary">Submit</button>
     </form>
     <small id="emailHelp" class="form-text text-muted" style="color:black!important;text-align:right!important;">Not registered yet? <a href="/signup" <span style="color:red!important">SIGN UP here</span></a></small>
      
</div>
@endsection
