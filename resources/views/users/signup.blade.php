
@include('header')
@extends('users.layout')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<style>
#frmCheckPassword {border-top:#F0F0F0 2px solid;background:#FAF8F8;padding:10px;}
.demoInputBox{padding:7px; border:#F0F0F0 1px solid; border-radius:4px;}

#password-strength-status {padding: 5px 10px;color: #FFFFFF; border-radius:4px;margin-top:5px;}
.medium-password{background-color: #E4DB11;border:#BBB418 1px solid;}
.weak-password{background-color: #FF6600;border:#AA4502 1px solid;}
.strong-password{background-color: #12CC1A;border:#0FA015 1px solid;}

#registrationFormAlert {padding: 5px 10px;color: #FFFFFF; border-radius:4px;margin-top:5px;}
.notmatch-password{background-color: red;border:#AA4502 1px solid;margin-top:5px;padding: 5px 10px;}
.match-password{background-color: green;border:#0FA015 1px solid;margin-top:5px;padding: 5px 10px;}
.rcorners2 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 500px;
  height: 750px;
  top: 70%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   
.rcorners3 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 500px;
  height: 200px;
  top: 50%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   
#bg {
  position: fixed; 
  top: 0; 
  left: 0; 
	
  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 100%;
  z-index:-100;
}

@media only screen and (max-width: 1024px){
    
    .rcorners2 {
        border-radius: 25px;
      background: #73AD21;
      color: white;
      padding: 15px;
      position: absolute;
      width: 600px;
      height: 700px;
      top: 40%;
      left: 50%;
      -ms-transform: translateX(-50%) translateY(-50%);
      -webkit-transform: translate(-50%,-50%);
      transform: translate(-50%,-50%);
    }   

    .rcorners3 {
    border-radius: 25px;
    background: #73AD21;
    color: white;
    padding: 15px;
    position: absolute;
    width: 500px;
    height: 200px;
    top: 20%;
    left: 50%;
    -ms-transform: translateX(-50%) translateY(-50%);
    -webkit-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
}   

    #bg {
      position: fixed; 
      top: 0; 
      left: 0; 
        
      /* Preserve aspet ratio */
      min-width: 100%;
      min-height: 1
    }

@media only screen and (max-width: 500px){
    
.h1, h1 {
    font-size: 1.5rem;
}
.rcorners2 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 300px;
  height: 750px;
  top: 60%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   
.rcorners3 {
    border-radius: 25px;
  background: #73AD21;
  color: white;
  padding: 15px;
  position: absolute;
  width: 300px;
  height: 200px;
  top: 30%;
  left: 50%;
  -ms-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}   

#bg {
  position: fixed; 
  top: 0; 
  left: 0; 
	
  /* Preserve aspet ratio */
  min-width: 100%;
  min-height: 1
}

@media only screen and (max-width: 400px){
    
    .rcorners2 {
        border-radius: 25px;
      background: #73AD21;
      color: white;
      padding: 15px;
      position: absolute;
      width: 300px;
      height: 750px;
      top: 70%;
      left: 50%;
      -ms-transform: translateX(-50%) translateY(-50%);
      -webkit-transform: translate(-50%,-50%);
      transform: translate(-50%,-50%);
    }   
    #bg {
      position: fixed; 
      top: 0; 
      left: 0; 
        
      /* Preserve aspet ratio */
      min-width: 100%;
      min-height: 1
    }

</style>
<img src="images/images3.jpg" id="bg" alt="">
@if ($users['errornya']=='cekemail')
<div class='rcorners3' >
@endif
@if ($users['errornya']!='cekemail')
<div class='rcorners2' >
@endif
    <h1>User Registration</h1>    
    &nbsp;<br>&nbsp;<br>    
    <form action="/registernew" method="post" class="needs-validation" novalidate>
    <div>
        @if (session()->has('message'))
          @if (session('message')=='User already registered')
              <div class="alert alert-danger" >
                  <span style="color:red"><b>*{{ session('message') }}</b></span>
              </div>
          @endif
          @if (session('message')=='Please check your email for confirmation')
              <div class="alert alert-success" >
                  <span style="color:red"><b>{{ session('message') }}</b></span>
              </div>
          @endif
        @endif
    </div>
    {{csrf_field()}}
    @csrf
    @if (session('message')!='Please check your email for confirmation')
        <div class="form-group">
            <label for="user_fullname">Full Name</label>
            <input required type="text" class="form-control" id="user_fullname" name="user_fullname" placeholder="Enter Fullname">
            
        </div>
        <div class="form-group">
            <label for="user_email">Email address</label>
            <input required type="email" class="form-control" id="user_email" name="user_email" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted" style="color:black!important;">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label  for="user_phone">Phone number</label>
            <div class="bfh-selectbox bfh-countries" data-country="US" data-flags="true">
</div>
            <input required type="tel" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Phone Number">
         </div>
        <div class="form-group">
            <label for="user_password">Password</label>
            <input required type="password" class="form-control" id="user_password" name="user_password" placeholder="Password"  onKeyUp="checkPasswordStrength();">
            <div id="password-strength-status"></div>
        </div>
        <div class="form-group">
            <label for="user_password2">Password Confirmation</label>
            <input required type="password" class="form-control" id="user_password2" name="user_password2" placeholder="Password" onKeyUp="checkPasswordMatch();">
            <div id="CheckPasswordMatch"></div>
        </div>
      <button disabled id="submitbtn" type="submit" class="btn btn-primary">Submit</button>
      &nbsp;&nbsp;&nbsp;
      <button type="button" class="btn btn-primary"  onclick="window.location.href='/';">Cancel</button>
     </form>
     @endif
</div>
@endsection
<script lang="javascript">

(function() {
'use strict';
window.addEventListener('load', function() {
// Fetch all the forms we want to apply custom Bootstrap validation styles to
var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
var validation = Array.prototype.filter.call(forms, function(form) {
form.addEventListener('submit', function(event) {
if (form.checkValidity() === false) {
event.preventDefault();
event.stopPropagation();
}
form.classList.add('was-validated');
}, false);
});
}, false);
})();

function checkPasswordStrength() {
 $("#user_password2").val('');
 checkPasswordMatch();
var number = /([0-9])/;
var alphabets = /([a-zA-Z])/;
var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
if($('#user_password').val().length<6) {
$('#password-strength-status').removeClass();
$('#password-strength-status').addClass('weak-password');
$('#password-strength-status').html("Weak (should be atleast 6 characters.)");
} else {  	
if($('#user_password').val().match(number) && $('#user_password').val().match(alphabets) && $('#user_password').val().match(special_characters)) {            
$('#password-strength-status').removeClass();
$('#password-strength-status').addClass('strong-password');
$('#password-strength-status').html("Strong");
} else {
$('#password-strength-status').removeClass();
$('#password-strength-status').addClass('medium-password');
$('#password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
}}}

function checkPasswordMatch() {
    var password = $("#user_password").val();
     var confirmPassword = $("#user_password2").val();
        if (password != confirmPassword)
        {
            $('#CheckPasswordMatch').removeClass();
            $('#CheckPasswordMatch').addClass('notmatch-password');
            $("#CheckPasswordMatch").html("Passwords does not match!");
            $("#submitbtn").prop("disabled",true);
        }
        else
        {
            $('#CheckPasswordMatch').removeClass();
            $('#CheckPasswordMatch').addClass('match-password');
            $("#CheckPasswordMatch").html("Passwords match.");
            $("#submitbtn").prop("disabled",false);
        }
    

}


</script>

