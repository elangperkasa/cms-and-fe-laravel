

@include('header')
<!-- <link rel="stylesheet" type="text/css" href="{{ URL::to('slick/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('slick/slick-theme.css') }}"> -->


   
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
<style>
  /* Make the image fully responsive */
  .carousel-inner img {
    width: 100%;
    height: 100%;
  }

  .carousel-inner > .item {
    position: relative;
    display: none;
    -webkit-transition: 0.6s ease-in-out left;
    -moz-transition: 0.6s ease-in-out left;
    -o-transition: 0.6s ease-in-out left;
    transition: 0.6s ease-in-out left;
}
  </style>
<style type="text/css">
            html, body {
              margin: 0;
              padding: 0;
            }
        
            * {
              box-sizing: border-box;
            }
        
        .lebar80
        {
          width:80%;
          margin: auto;
        }
            .slider {
                width: 80%;
                margin: 100px auto;
            }
        
            .slick-slide {
              margin: 0px 20px;
              height: 150px;

            }
        
            .slick-slide img {
              width: 100%;
            }
        
            .slick-prev:before,
            .slick-next:before {
              color: black;
            }
        
        
            .slick-slide {
              transition: all ease-in-out .3s;
              opacity: 1;
            }
            
            .slick-active {
              opacity: 1;
            }
        
            .slick-current {
              opacity: 1;
            }
        
            .slick-prev:before, .slick-next:before {
                display: none;
            }
            /* .slick-dots
            {
                display: none!important;
            } */
            .slick-prev, .slick-next {
              display: none;
            }

                        .slick-slide {
                margin: 0px 5px;
                height: 150px;
            }

            .slider {
                width: 80%;
                margin: 50px auto;
            }
            
            .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    padding-right: 5px;
    padding-left: 5px;
    padding-bottom: 8px;
}



.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  border-radius: 5px; /* 5px rounded corners */
}

/* Add rounded corners to the top left and the top right corner of the image */
img {
  border-radius: 5px 5px 0 0;
}

@media only screen and (max-width: 1024px) {

.slider {
    width: 80%;
    margin: 10px auto;
}
.card-text1 {
  font-size:.7em;
}

.slick-slide {
    margin: 15px 15px;
    height: 100px;
}
  
}

@media only screen and (max-width: 600px) {

    .slider {
        width: 80%;
        margin: 10px auto;
    }
    .card-text1 {
      font-size:.7em;
    }

    .slick-slide {
        margin: 0px 5px;
        height: 50px;
    }

    .h5, h5 {
    font-size: 1rem;
}

.card-body {
   
    padding: .5rem;
}
}




</style>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
 
  <ol class="carousel-indicators">
  <!-- @foreach ($users['banner'] as $banners)
      <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
   @endforeach -->
  </ol>
 
  <div class="carousel-inner" role="listbox">
      @foreach ($users['banner'] as $banners)
          <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
              <img class="d-block img-fluid" src="{{ asset('/uploads/'.$banners->banner_image) }}" alt="{{ $banners->banner_name }}"  width="1100" height="500">
                  <div class="carousel-caption d-none d-md-block">
                    <!-- <h3>{{ $banners->banner_name }}</h3>                  -->
                  </div>
          </div>
        @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<!-- crousel 4  -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="http://placehold.it/350x100/FFFF00/000000?text=1" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://placehold.it/350x100/FF0000/000000?text=2" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="http://placehold.it/350x100/0000FF/000000?text=3" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


<div class="center slider  pointer">
            <div>
              <img src="http://placehold.it/350x300/FFFF00/000000?text=1">
            </div>
            <div>
              <img src="http://placehold.it/350x300/0000FF/808080?text=2">
            </div>
            <div>
              <img src="http://placehold.it/350x300/FF0000/FFFFFF?text=3">
            </div>
            <div>
              <img src="http://placehold.it/350x300/0000FF/808080?text=4">
            </div>
            <div>
              <img src="http://placehold.it/350x300/FF0000/FFFFFF?text=5">
            </div>
            <div>
              <img src="http://placehold.it/350x300/0000FF/808080?text=6">
            </div>
            <div>
              <img src="http://placehold.it/350x300/FF0000/FFFFFF?text=7">
            </div>
            <div>
              <img src="http://placehold.it/350x300/0000FF/808080?text=8">
            </div>
            <div>
              <img src="http://placehold.it/350x300?text=9">
            </div>
</div>

<div class="lebar80">
  <div class="row pointer">
      <div class="col-6 col-sm-6 col-xl-3">
          <div class="card">
            <img
              src="https://mdbootstrap.com/img/new/standard/nature/184.jpg"
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text1">
                Some quick example text to build on the card title and make up the bulk of the
                card's content. <br><br>sasa llllla  asaja ja j ja sjas ajs j j sja jiiui wiuiu i iui uiu 
              </p>
              <a href="#!" class="btn btn-primary">Button</a>
            </div>
          </div>
      </div>
      <div class="col-6 col-sm-6 col-xl-3">
          <div class="card">
            <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
              <img
                src="https://mdbootstrap.com/img/new/standard/nature/111.jpg"
                class="img-fluid"
              />
              <a href="#!">
                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
              </a>
            </div>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text1">
                Some quick example text to build on the card title and make up the bulk of the
                card's content.
              </p>
              <a href="#!" class="btn btn-primary">Button</a>
            </div>
          </div>
      </div>
      <div class="col-6 col-sm-6 col-xl-3">
          <div class="card">
            <img
              src="images/images.jpg"
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text1">
                Some quick example text to build on the card title and make up the bulk of the
                card's content.
              </p>
              <a href="#!" class="btn btn-primary">Button</a>
            </div>
          </div>
      </div>
      <div class="col-6 col-sm-6 col-xl-3">
          <div class="card">
            <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
              <img
                src="images/images2.jpg"
                class="img-fluid"
              />
              <a href="#!">
                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
              </a>
            </div>
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text1">
                Some quick example text to build on the card title and make up the bulk of the
                card's content.
              </p>
              <a href="#!" class="btn btn-primary">Button</a>
            </div>
          </div>
      </div>         
  </div>
  <br>&nbsp;
  <div class="row">
    <div class="col-sm-12 col-xl-6 col-lg-6 ">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">
                Some quick example text to build on the card title and make up the bulk of the
                card's content.
              </p>
              <button type="button" class="btn btn-primary">Button</button>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-xl-6 col-lg-6">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">
                  Some quick example text to build on the card title and make up the bulk of the
                  card's content.
                </p>
                <button type="button" class="btn btn-primary">Button</button>
              </div>
            </div>
        </div>  
    </div>
  </div>
</div>
&nbsp;<br>&nbsp;<br>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::to('slick/slick.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.scroller').css('height', $(window).height() + 'px');
});

$(document).on('ready', function() {     
    $(".center").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
        
    });

    $(window).resize(function() {
      $('.js-slider').slick('resize');
    });

    $(window).on('orientationchange', function() {
      $('.js-slider').slick('resize');
    });

});
</script>