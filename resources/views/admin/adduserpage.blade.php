@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
  text-align: left!important;
  float: left;
  padding-left:20px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<div class="isipage scroll">     
        <div class="container">  
        <h1>Add New User</h1>    
            &nbsp;<br>&nbsp;<br>    
            <form action="/insertnewuser" method="post">
            {{ csrf_field() }}
            <div>
                @if (session()->has('message'))
                @if (session('message')=='User already registered')
                    <div class="alert alert-danger" >
                        <span style="color:red"><b>*{{ session('message') }}</b></span>
                    </div>
                @endif
                @if (session('message')=='Please check your email for confirmation')
                    <div class="alert alert-success" >
                        <span style="color:red"><b>{{ session('message') }}</b></span>
                    </div>
                @endif
                @endif
            </div>
           
            @if (session('message')!='Please check your email for confirmation')
           
            <input type="hidden" id="user_id" name="user_id" >
                   
                <div class="row">
                    <div class="col-25">
                        <label for="user_name">Full Name</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter Fullname" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_email">Email address</label>
                     </div>
                    <div class="col-75">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Enter email"  >
                     </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_phone">Phone number</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Phone Number"  >
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_type">User Type</label>
                    </div>
                    <div class="col-75">
                        <select name="user_type" id="user_type">  
                            <option value="staff">staff</option>                            
                            <option value="admin">admin</option>                            
                            <option value="user">user</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_status">User Status</label>
                    </div>
                    <div class="col-75">
                        <select name="user_status" id="user_status">  
                            <option value="not active">not active</option>
                            <option value="active">active</option>
                            <option value="register">register</option>
                           
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_password">Password</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="user_password" name="user_password" placeholder="Enter Password"  >
                    </div>
                </div>
                &nbsp;<br>&nbsp;<br>
            <button type="submit" class="btn btn-primary">Submit</button>
            &nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-primary"  onclick="window.location.href='userlist';">Cancel</button>
            </form>
            @endif
        </div>
</div>
@stop