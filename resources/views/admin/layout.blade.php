<!doctype html>
<html>
<head>
    <style>
        .isipage
        {
            position : absolute;
            /* z-index: 1000; */
            top : 25px;
            left : 300px;
            margin: auto;
            background-color: #20B2AA;
            opacity: 0.9;
            width: 100px;
        }
      table,
      tr,
      td {
        padding: 10px;
        border: 1px solid white;
        border-collapse: collapse;
        color: white;;
      }

      div.scroll {
        width: 1017px;
        height: 600px;
        overflow-x: hidden;
        overflow-y: auto;
        text-align: center;
        padding: 20px;
        }

        .pointer
        {
        cursor: pointer
        }

    </style>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
@include('admin.sidebar')
<div id="main" class="rowcontent">
           @yield('content')
</div>
   <footer class="row">
       @include('admin.footer')
   </footer>  
</div>
</body>
