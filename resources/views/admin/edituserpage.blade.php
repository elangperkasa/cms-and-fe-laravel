@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
  text-align: left!important;
  float: left;
  padding-left:20px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<div class="isipage scroll">     
        <div class="container">  
        <h1>Edit User Data</h1>    
            &nbsp;<br>&nbsp;<br>    
            <form action="/updateuser" method="post">
            {{csrf_field()}}
            <div>
                @if (session()->has('message'))
                @if (session('message')=='User already registered')
                    <div class="alert alert-danger" >
                        <span style="color:red"><b>*{{ session('message') }}</b></span>
                    </div>
                @endif
                @if (session('message')=='Please check your email for confirmation')
                    <div class="alert alert-success" >
                        <span style="color:red"><b>{{ session('message') }}</b></span>
                    </div>
                @endif
                @endif
            </div>
           
            @if (session('message')!='Please check your email for confirmation')
            @foreach ($users['usernya'] as $user)
            <input type="hidden" id="user_id" name="user_id"  value="{{$user->user_id}}">
                   
                <div class="row">
                    <div class="col-25">
                        <label for="user_fullname">Full Name</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter Fullname" value="{{$user->user_name}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_email">Email address</label>
                     </div>
                    <div class="col-75">
                        <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Enter email"  value="{{$user->user_email}}">
                     </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_phone">Phone number</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Phone Number"  value="{{$user->user_phone}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_type">User Type</label>
                    </div>
                    <div class="col-75">
                        <select name="user_type" id="user_type">  
                            <option value="admin" 
                            <?php if ( $user->user_type =='admin') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >admin</option>
                            <option value="staff"
                            <?php if ( $user->user_type=='staff') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >staff</option>
                            <option value="user"
                            <?php if ( $user->user_type=='user') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >user</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="user_status">User Status</label>
                    </div>
                    <div class="col-75">
                        <select name="user_status" id="user_status">  

                            <option value="active" 
                            <?php if ( $user->user_status =='active') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >active</option>
                            <option value="register"
                            <?php if ( $user->user_status=='register') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >register</option>
                            <option value="not active"
                            <?php if ( $user->user_status=='not active') 
                            {
                                echo 'selected';
                            }
                            ?>
                            >not active</option>
                        </select>
                    </div>
                </div>
 
                &nbsp;<br>&nbsp;<br>
            <button type="submit" class="btn btn-primary">Submit</button>
            &nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-primary"  onclick="window.location.href='userlist';">Cancel</button>
            </form>
            @endforeach
            @endif
        </div>
</div>
@stop