<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Travel - Cheap</title>
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <style>
      .hidden {
      list-style-type: none!important;
      display: none!important;
      }
      ul.submenu {
        list-style-position: inside;
        list-style-type: none!important;
      }

      /* ul.submenu
      {
      list-style-type: none!important;
      display: none!important;
      } */
      .submenumaster
      {
      list-style-type: none!important;
      display: none!important;
      }
      
      .submenucontent
      {
      list-style-type: none!important;
      display: none!important;
      }

      .showme
      {
        display: block!important;
        list-style-type: none!important;
      }

      .hideme
      {
        display: none!important;
        list-style-type: none!important;
      }

      .main_box {
         
          height: 130vh;
      }
      .main_box .sidebar_menu {
          height: 120vh;
          position: absolute;
          /* position: fixed; */
      }

      .menu
      {
        color : white;
        /* font-weight: bold; */
        font-size : 1.2em;
        cursor: pointer;
      }


    </style>
   </head>
<body>
  <div class="main_box">
    <input type="checkbox" id="check" checked>
    <div class="btn_one">
      <label for="check">
        <i class="fas fa-bars burger"></i>
      </label>
    </div>
    <div class="sidebar_menu">
      <div class="logo">
        <a href="#">Travel Cheap</a>
          </div>
        <div class="btn_two">
          <label for="check">
            <i class="fas fa-times burger2"></i>
          </label>
        </div>
      <div class="menu">
        <ul class="hiddenx">
          <li>
            <!-- <i class="fas fa-qrcode"></i> -->
            <a href="admin">Dashboard</a>
          </li>
          <li>
            <!-- <i class="fas fa-link"></i> -->
            <span class="master">Master</span> 
            <ul class="submenumaster" id="submenumaster">
                <li><i class="fas fa-id-card"></i>
                    <a href="#">Role</a>
                </li>
                <li><i class="far fa-handshake"></i>
                    <a href="#">Priviledge</a>
                </li>
                <li><i class="fas fa-user"></i>
                    <a href="#">User Type</a>
                </li>
            </ul>      
          </li>

          <li>
            <!-- <i class="fas fa-stream"></i> -->
            <a href="userlist">User List</a>
          </li>
          <li>
            <!-- <i class="fas fa-calendar-week"></i> -->            
            <span class="content">Content</span> 
            <ul class="submenucontent" id="submenucontent">
                <li><i class="fa fa-object-group"></i>
                    <a href="/banner">Banner</a>
                </li>
                
                <li><i class="fas fa-icons"></i>
                    <a href="/products">Products</a>
                </li>
            </ul>      
          </li>
          <li>
            <!-- <i class="fas fa-question-circle"></i> -->
            <a href="#">Report</a>
          </li>
          <li>
            <!-- <i class="fas fa-sliders-h"></i> -->
            <a href="#">Log</a>
          </li>
          <li>
            <!-- <i class="fas fa-phone-volume"></i> -->
            <a href="/sysparam">system Parameter</a>
          </li>
          <li>
          <!-- <i class="far fa-comments"></i> -->
            <a href="logout">Logout</a>
          </li>
        </ul>
      </div>
      <!-- <div class="social_media">
        <ul>
          <a href="#"><i class="fab fa-facebook-f"></i></a>
          <a href="#"><i class="fab fa-twitter"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
          <a href="#"><i class="fab fa-youtube"></i></a>
        </ul>
      </div> -->
&nbsp;<br>&nbsp;<br>
    </div>
  </div>
  </body>

</html>
<script language=>
$(document).ready(function(){
 

    $('.burger2').click(function(){
        $("div.scroll").css("width", "1250px");
        $(".isipage").css("left", "50px");
        
    });

    $('.burger').click(function(){
        $("div.scroll").css("width", "1017px");
        $(".isipage").css("left", "300px");        
    });

    $('.master').click(function()
    {      
     
      $('#submenumaster').removeClass();
      $('#submenumaster').addClass('showme');

      $('#submenucontent').removeClass();
      $('#submenucontent').addClass('hideme');
    });

    $('.content').click(function()
    {      
      // alert("ok");
      $('#submenumaster').removeClass();
      $('#submenumaster').addClass('hideme');

      $('#submenucontent').removeClass();
      $('#submenucontent').addClass('showme');


    });
    
});
</script>