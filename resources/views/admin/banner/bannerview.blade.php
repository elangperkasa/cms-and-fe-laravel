@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<div class="isipage scroll">
<h1> Banner List </h1>
        <button type="button" class="btn " style="align:right;float:right;background-color:white;" onclick="location.href='addbanner'">Add New Banner</button>&nbsp;<br>&nbsp;<br>
<table border = "1" class="table">
<thead>
<tr class="thead-dark">    
<th  scope="col">Banner Name</th>
<th  scope="col">Banner Click To</th>
<th  scope="col">Banner Image</th>
<th  scope="col">Action</th>
</tr>
</thead>
@foreach ($users['users'] as $users)
<tbody>
<tr>
<th  scope="row">{{ $users->banner_name }}</th>
<th  scope="row">{{ $users->banner_url }}</th>
<th  scope="row">{{ $users->banner_image }}</th>
<th  scope="row">
    <a href="editbanner?id={{ $users->id }}"><i class="fa fa-edit blue-color pointer" ></i></a>
    &nbsp;&nbsp;<a href="deletebanner?id={{ $users->id }}"><i class="fa fa-trash  blue-color pointer" aria-hidden="true"></i>
</th>
</tr>
</tbody>
@endforeach
</table>
<br>

</div>
@stop