@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<div class="isipage scroll">
<h1> Parameter List </h1>
        <button type="button" class="btn " style="align:right;float:right;background-color:white;" onclick="location.href='addparam'">Add Parameter</button>&nbsp;<br>&nbsp;<br>
<table border = "1" class="table">
<thead>
<tr class="thead-dark">   
<th  scope="col">Parameter Name</th>
<th  scope="col">Value</th>
<th  scope="col">Action</th>
</tr>
</thead>
@foreach ($users['data'] as $data)
<tbody>
<tr>
<th  scope="row">{{ $data->param_name }}</th>
<th  scope="row">{{ $data->param_value }}</th>
<th  scope="row">
    <a href="editparam?id={{ $data->id }}"><i class="fa fa-edit blue-color pointer" ></i></a>
    &nbsp;&nbsp;<a href="deleteparam?id={{ $data->id }}"><i class="fa fa-trash  blue-color pointer" aria-hidden="true"></i>
</th>
</tr>
</tbody>
@endforeach
</table>
@stop