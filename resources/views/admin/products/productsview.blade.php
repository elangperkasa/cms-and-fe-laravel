@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<div class="isipage scroll">
<h1> Products List </h1>
        <button type="button" class="btn " style="align:right;float:right;background-color:white;" onclick="location.href='addproduct'">Add New Product</button>&nbsp;<br>&nbsp;<br>
<table border = "1" class="table">
<thead>
<tr class="thead-dark">    
<th  scope="col">Product Name</th>
<th  scope="col">Product Price</th>
<th  scope="col">Product Image</th>
<th  scope="col">Product Description</th>
<th  scope="col">Product Type</th>
<th  scope="col">Product Active </th>
<th  scope="col">Action</th>
</tr>
</thead>
@foreach ($users['products'] as $users)
<tbody>
<tr>
<th  scope="row">{{ $users->product_name }}</th>
<th  scope="row">{{ $users->product_price }}</th>
<th  scope="row">
<!-- {{ $users->product_image }} -->
<img src="{{ asset('/uploads/'.$users->product_image) }}"  width="50" height="30">
                   
</th>
<th  scope="row">{{ $users->product_description }}</th>
<th  scope="row">Slide: {{ $users->smallslide }}<br>Cards: {{ $users->cards }}<br>Article: {{ $users->article }}</th>
<th  scope="row">{{ $users->active }}</th>

<th  scope="row">
    <a href="editproducts?id={{ $users->id }}"><i class="fa fa-edit blue-color pointer" ></i></a>
    &nbsp;&nbsp;<a href="deleteproducts?id={{ $users->id }}"><i class="fa fa-trash  blue-color pointer" aria-hidden="true"></i>
</th>
</tr>
</tbody>
@endforeach
</table>
<br>

</div>
@stop