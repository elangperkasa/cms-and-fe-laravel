@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
  text-align: left!important;
  float: left;
  padding-left:20px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
<div class="isipage scroll">     
        <div class="container">  
        <h1>Edit User Data</h1>    
            &nbsp;<br>&nbsp;<br>    
            <form action="/updateproduct" method="post"  enctype="multipart/form-data">
            {{csrf_field()}}
            <div>
                @if (session()->has('message'))
                @if (session('message')=='User already registered')
                    <div class="alert alert-danger" >
                        <span style="color:red"><b>*{{ session('message') }}</b></span>
                    </div>
                @endif
                @if (session('message')=='Please check your email for confirmation')
                    <div class="alert alert-success" >
                        <span style="color:red"><b>{{ session('message') }}</b></span>
                    </div>
                @endif
                @endif
            </div>
           
            @if (session('message')!='Please check your email for confirmation')
            @foreach ($users['usernya'] as $user)
            <input type="hidden" id="id" name="id"  value="{{$user->id}}">
                   
                <div class="row">
                    <div class="col-25">
                        <label for="product_name">Product Name</label>
                    </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name" value="{{$user->product_name}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="product_price">Product Price</label>
                     </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="product_price" name="product_price" placeholder="product_price"  value="{{$user->product_price}}">
                     </div>
                </div>
                <div class="row">    
                      <div class="col-25">
                            <label for="title">Product Image</label>
                            </div>
                    <div class="col-75">
                                <input type="file" name="files[]" class="form-control-file" multiple="">
                                @if($errors->has('files'))
                                    <span class="help-block text-danger">{{ $errors->first('files') }}</span>
                                @endif  
                                </div>   
                </div> 
                <div class="row">
                    <div class="col-25">
                    </div>
                    <div class="col-75"  style="text-align: left!important;padding:10px">
                    <img src="{{ asset('/uploads/'.$user->product_image) }}"  width="100" height="100">
                        </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="product_description">Product Description</label>
                     </div>
                    <div class="col-75">
                        <input type="text" class="form-control" id="product_description" name="product_description" placeholder="product_description"  value="{{$user->product_description}}">
                     </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="smallslide">Product Type</label>
                     </div>
                    <div class="col-75"  style="text-align: left!important;padding:10px">
                        <input type="checkbox" name="smallslide" value="Y" 
                        @if ($user->smallslide=="Y") 
                        {
                          checked
                        }
                        @endif
                        > Small Slide &nbsp; &nbsp; &nbsp;
                        <input type="checkbox" name="cards" value="Y" 

                        @if ($user->cards=="Y") 
                        {
                          checked
                        }
                        @endif
                        > Cards  &nbsp; &nbsp; &nbsp;
                        <input type="checkbox" name="article" value="Y" 
                        @if ($user->article=="Y") 
                        {
                          checked
                        }
                        @endif
                        
                        > Article  &nbsp; &nbsp; &nbsp;
                        </div>
                </div> 
                <div class="row">
                    <div class="col-25">
                        <label for="active">Product Status</label>
                     </div>
                    <div class="col-75" style="text-align: left!important;padding:10px">
                        <input type="checkbox" name="active" value="Y"                         
                        @if ($user->active=="Y") 
                        {
                          checked
                        }
                        @endif
                        > Activated
                        
                    </div>
                </div>
                &nbsp;<br>&nbsp;<br>
            <button type="submit" class="btn btn-primary">Submit</button>
            &nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-primary"  onclick="window.location.href='products';">Cancel</button>
            </form>
            @endforeach
            @endif
        </div>
</div>
@stop