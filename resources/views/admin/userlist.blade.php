@extends('admin.layout')

@section('title', 'User List')
@section('sidebar')
@parent <!-- Includes parent sidebar -->

@stop

@section('content')
<div class="isipage scroll">
<h1> User List </h1>
        <button type="button" class="btn " style="align:right;float:right;background-color:white;" onclick="location.href='adduser'">Add New User</button>&nbsp;<br>&nbsp;<br>
<table border = "1" class="table">
<thead>
<tr class="thead-dark">    
<th  scope="col">No</th>
<th  scope="col">Email</th>
<th  scope="col">Password</th>
<th  scope="col">User Type</th>
<th  scope="col">User Status</th>
<th  scope="col">Action</th>
</tr>
</thead>
@foreach ($users['usernya'] as $user)
<tbody>
<tr>
<th  scope="row">
@if($users['page'] != 1 && $users['page']!=null)  
    {{ $users['page']-1 }}{{$loop->iteration }}
@else   
    {{$loop->iteration }}
@endif
</th>
<th  scope="row">{{ $user->user_email }}</th>
<th  scope="row">{{ $user->user_password }}</th>
<th  scope="row">{{ $user->user_type }}</th>
<th  scope="row">{{ $user->user_status }}</th>
<th  scope="row">
    <a href="edituser?id={{ $user->user_id }}"><i class="fa fa-edit blue-color pointer" ></i></a>
    &nbsp;&nbsp;<a href="deleteuser?id={{ $user->user_id }}"><i class="fa fa-trash  blue-color pointer" aria-hidden="true"></i>
</th>
</tr>
</tbody>
@endforeach
</table>
<div align=left>
<span style="color:white;text-align: left!important;">Total Record {{$users['count']}} Row</span>
</div><br>
{{$users['usernya']->links() }} 
</div>
@stop