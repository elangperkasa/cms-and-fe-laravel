<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
    @include('admin.sidebar')
        @show
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>