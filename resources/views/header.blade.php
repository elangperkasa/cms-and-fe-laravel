<style>
    .pointer
        {
        cursor: pointer
        }
</style>
        <!-- isinya = {{--$users['login']--}} -->
        <div class="header">
            <a href="/"><i class="fa fa-home" aria-hidden="true"></i> Home</a>  
            @if($users['login'] =='done')  
            &nbsp;&nbsp;<a href="/profile"><i class="fa fa-user-circle" aria-hidden="true"></i> Profile</a>                   
                &nbsp;&nbsp;<a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
            @else
                &nbsp;&nbsp;<a href="/loginpage"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
            @endif
        </div>
  