<style>
.button {
  /* background-color: #4CAF50; Green */
  background-color: #008CBA;  /* blue */
  border: none;
  color: white;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.button1 {border-radius: 2px;}
.button2 {border-radius: 4px;
    }
.button3 {border-radius: 8px;}
.button4 {border-radius: 12px;}
.button5 {border-radius: 50%;}
</style>

<h1>Hi, {{ $name }}  The Traveller</h1>
<p>Please click this url to confirm your email</p>

<a href="{{ $url }}"><button class="button button2">Confirm my email</button></a>&nbsp;<br>&nbsp;<br>
or&nbsp;<br>&nbsp;<br>
<a href="{{ $url }}">klik this for confirmation</a>&nbsp;<br>&nbsp;<br>

or copy paste this url below to your browser&nbsp;<br>&nbsp;<br>

{{ $url }}
<br><br>
<b>Thanks<br>
Travel Cheap - Always</b>